package fr.rest.api.resources.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel(value = "Account", description = "Account")
public class AccountDto {

    @NotNull
    private String accountNumber;

    @NotNull
    private String owner;

    public AccountDto() {
    }

    public AccountDto(String accountNumber, String owner) {
        this.accountNumber = accountNumber;
        this.owner = owner;
    }

    @ApiModelProperty(position = 1, required = true, value = "account number")
    public String getAccountNumber() {
        return accountNumber;
    }

    @ApiModelProperty(position = 2, required = true, value = "Owner of account number")
    public String getOwner() {
        return owner;
    }
}