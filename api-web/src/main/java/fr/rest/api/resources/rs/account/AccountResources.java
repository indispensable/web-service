package fr.rest.api.resources.rs.account;

import fr.core.exception.AccountBusinessException;
import fr.core.services.AccountService;
import fr.rest.api.exception.HelpException;
import fr.rest.api.resources.model.AccountDto;
import fr.rest.api.resources.model.AccountListRequest;
import fr.rest.api.resources.model.AccountRequest;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.httpclient.HttpStatus.*;

/**
 * REST endpoint for Account manipulation.
 */
@Api(value = "Account")
@Component(value = "accountResources")
@Path("/v1/account")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResources {

    private static Logger LOGGER = LoggerFactory.getLogger(AccountResources.class);

    @Autowired
    private AccountService accountService;

    @GET
    @Path("/details")
    @ApiOperation(value = "Finds account details for given account number", response = AccountDto.class)
    @RequestMapping(method = RequestMethod.GET, path = "/details", produces = MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successful retrieval of account detail", response = AccountDto.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "Account with given username does not exist"),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error") })
    @RolesAllowed("Amara")
    public Response getAccountDetails() {

        LOGGER.debug("Get account details.");
        return Response.ok().entity(new AccountDto("1000123", "Amara TAF")).build();
    }

    @GET
    @Path("/account/{accountNumber}/details")
    @ApiOperation(value = "Finds account details for given account number", response = AccountDto.class)
    public Response getAccountDetails(
            @ApiParam(name = "accountNumber", value = "Integer account number", required = true)
            @PathParam("accountNumber") String accountNumber) {

        LOGGER.debug("Fin account details for given account number {}", accountNumber);
        try {
            // call Module process of Service.
            accountService.findDetails(accountNumber);
            // must transform business exception to rest web services exception.
            // good practice. Return code message and domain of this error.
        } catch (AccountBusinessException e) {
            LOGGER.error("Error occurring when calling get account details for given account {}", accountNumber, e);
            // We can do this by other way. We just add provider :
            // add : ExampleExceptionMapper implements ExceptionMapper<Exception> {
            switch (e.getError().name()) {
            case "ACCOUNT_NOT_FOUND":
                return Response.status(Response.Status.NOT_FOUND).entity(new HelpException(e)).build();
            default:
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new HelpException(e)).build();
            }
        }
        return Response.ok(new AccountDto(accountNumber, "AMARA")).build();
    }

    // Post method : post parameters for post can be in Objects request. ex : AccountRequest.
    // If one param we can make it without create objects request.

    @POST // One param
    @Path("/activeAccount")
    @ApiOperation(value = "Active account for given account number and status.")
    public Response activeAccount(@ApiParam(value = "Account number", required = true) String accountNumber) {

        LOGGER.debug("active Account for given account {} and given status {}", accountNumber);
        return Response.ok("").build();
    }

    @POST // two param
    @Path("/activeAccountWith")
    @ApiOperation(value = "Active account for given account number and status.")
    public Response activeAccount(@ApiParam(value = "Account Request", required = true) AccountRequest accountRequest) {

        LOGGER.debug("active Account for given account {} and given status {}", accountRequest.getAccountNumber(), accountRequest.getStatus());
        return Response.ok(new AccountDto(accountRequest.getAccountNumber(), accountRequest.getStatus())).build();
    }

    @POST // List in ObjectRequest
    @Path("/activeAccountListInObjectRequest")
    @ApiOperation(value = "Creates list of users with given input array")
    public Response createUsersWithArrayInput(@ApiParam(value = "List of user account", required = true) AccountListRequest accountListRequest)  {

        List<AccountDto> myAccounts = new ArrayList<>();
        for (AccountDto accountDto: accountListRequest.getAccounts()) {
            myAccounts.add(accountDto);
        }
        return Response.ok().entity(myAccounts).build();
    }

    /**
     To test this in swagger we can use json object :
     [{"accountNumber": "TOTO","owner": "TOTO"}]
     */
    @POST // List in request
    @Path("/activeAccountListInRequest")
    @ApiOperation(value = "Creates list of users with given input array")
    public Response createUsersWithArrayInput(@ApiParam(value = "List of user account", required = true) List<AccountDto> accounts)  {

        List<AccountDto> myAccounts = new ArrayList<>();
        for (AccountDto accountDto: accounts) {
            myAccounts.add(accountDto);
        }
        // We can return a list.
        return Response.ok().entity(myAccounts).build();
    }


    @ApiParam(value = "Device Type", defaultValue = "SamsungGalaxyS4", required = true)
    @HeaderParam("authorization") String authorization;
    @GET
    @Path("/detailsWithAuthorization")
    @ApiOperation(value = "Finds account details for given account number", response = AccountDto.class)
    @RequestMapping(method = RequestMethod.GET, path = "/details", produces = MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successful retrieval of account detail", response = AccountDto.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "Account with given username does not exist"),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error") })
    @RolesAllowed("Amara")

    public Response getDetailsWithAuthorization() {

        LOGGER.debug("Get account details.");
        return Response.ok().entity(new AccountDto("1000123", "Amara TAF")).build();
    }
}
