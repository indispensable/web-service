package fr.rest.api.exception;

import fr.core.exception.AccountBusinessException;
import fr.core.exception.BusinessException;
import io.swagger.annotations.ApiModel;

import javax.ws.rs.WebApplicationException;

/**
 * Base Class for all exception thrown by rest ws resources.
 * <p>
 * Help exception : Is generic class exception.
 * each resources does throw this exception not business exception.
 */
@ApiModel(value = "HelpException", description = "HelpException")
public class HelpException {
    /**
     * Error code.
     */
    private final String code;

    /**
     * Error message. explain this error.
     */
    private final String message;
    /**
     * Functional domain.
     */
    private final String functionalDomain;

    public HelpException(String code, String message, String functionalDomain) {
        this.code = code;
        this.functionalDomain = message;
        this.message = message;
    }

    public HelpException(AccountBusinessException e) {
        this(e.getCode(), e.getMessage(), e.getFunctionalDomain());
    }

    /**
     * @return Business error code.
     */
    public String getCode() {
        return code;
    }

    /**
     * @return Business error Functional domain.
     */
    public String getFunctionalDomain() {
        return functionalDomain;
    }

    /**
     * @return message of error.
     */
    public String getMessage() {
        return message;
    }
}
