package fr.core.dao;

import fr.core.exception.AccountBusinessException;
import fr.core.model.Account;

public interface AccountDao {
    /**
     * Find account details.
     *
     * @param accountNumber
     * @return Account details.
     */
    public Account findAccountDetails(String accountNumber);
}
