package fr.core.exception;

public enum TechnicalExceptionError {

    INTERNAL_ERROR, HELP_ERROR, INVALID_REQUEST, INVALID_HEADER, INVALID_USERKEY;
}
