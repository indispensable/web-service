package fr.core.exception;

public class TechnicalException extends RuntimeException {

    private final TechnicalExceptionError errorCode;

    public TechnicalException() {
        super();
        errorCode = TechnicalExceptionError.INTERNAL_ERROR;
    }

    public TechnicalException(String message) {
        super(message);
        errorCode = TechnicalExceptionError.INTERNAL_ERROR;
    }

    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
        errorCode = TechnicalExceptionError.INTERNAL_ERROR;
    }

    public TechnicalException(TechnicalExceptionError errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public TechnicalException(TechnicalExceptionError anErrorCode, Throwable cause) {
        super(cause);
        this.errorCode = anErrorCode;
    }

    public TechnicalException(String message, TechnicalExceptionError anErrorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = anErrorCode;
    }

    public TechnicalExceptionError getErrorCode() {
        return errorCode;
    }

    /*


    example :

    throw new TechnicalException(TechnicalExceptionError.HELP_ERROR, e);

     */
}
