package fr.core.exception;

/**
 * Abstract representation of business error case.
 * Contain :
 * - error code name (name) : can be used to take decision in client (in services or processes.
 * - message : Comprehensive details about error.
 */
public interface BusinessError{

    /**
     * @return error message.
     */
    String getMessage();

    /**
     * @return name of Business error.
     */
    String name();
}
