package fr.core.exception;

public class AccountBusinessException extends BusinessException {

    public enum AccountBusinessError implements BusinessError {

        ACCOUNT_NOT_FOUND("account not found"), ACCOUNT_BUSINESS_ERROR("Generic errors");

        private final String message;

        AccountBusinessError(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

    public AccountBusinessException(BusinessError error, String message, String functionalDomain) {
        super(error, message, functionalDomain);
    }

}
