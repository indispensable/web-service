package fr.core.exception;

/**
 * This class
 */
public abstract class BusinessException extends Exception {
    /**
     *
     */
    private final BusinessError error;

    /**
     * Error code.
     */
    private final String code;

    /**
     * Functional domain.
     */
    private final String functionalDomain;

    /**
     * @param error
     * @param message
     * @param functionalDomain
     */
    public BusinessException(BusinessError error, String message, String functionalDomain) {
        super(message);
        this.error = error;
        this.code = error.name();
        this.functionalDomain = functionalDomain;
    }

    /**
     * @param error
     * @param cause
     * @param functionalDomain
     */
    public BusinessException(BusinessError error, Throwable cause, String functionalDomain) {
        super(cause);
        this.error = error;
        this.code = error.name();
        this.functionalDomain = functionalDomain;
    }

    /**
     * @param error
     * @param cause
     * @param functionalDomain
     */
    public BusinessException(BusinessError error, String message, Throwable cause, String functionalDomain) {
        super(message, cause);
        this.error = error;
        this.code = error.name();
        this.functionalDomain = functionalDomain;
    }

    public BusinessError getError() {
        return error;
    }

    public String getCode() {
        return code;
    }

    public String getFunctionalDomain() {
        return functionalDomain;
    }
}
