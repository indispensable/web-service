package fr.core.services;

import fr.core.dao.AccountDao;
import fr.core.exception.AccountBusinessException;
import fr.core.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static fr.core.exception.AccountBusinessException.AccountBusinessError;

@Service
public class DefaultAccountService implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Override
    public Account findDetails(String accountNumber) throws AccountBusinessException {
        Account account = accountDao.findAccountDetails(accountNumber);
        if (account == null) {
            // A functional error. i can return account not found. and in frond for example i can display a message.
            throw new AccountBusinessException(AccountBusinessError.ACCOUNT_NOT_FOUND, "account not exist", "Account Domain");
        }
        return new Account("123", "Amara");
    }
}
