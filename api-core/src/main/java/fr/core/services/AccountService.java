package fr.core.services;

import fr.core.exception.AccountBusinessException;
import fr.core.model.Account;

public interface AccountService {
    /**
     * Find account details.
     *
     * @param accountNumber
     * @return account details.
     * @throws AccountBusinessException
     */
    public Account findDetails(String accountNumber) throws AccountBusinessException;
}
