package com.oodrive;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Iterator;

public class A {

    /**
     * File name
     */
    private final static String FILE_NAME = "universe-formula";

    /**
     * directory path
     */
    private final static String DIRECTORY_PATH = "universe-formula";

    /**
     * Find formula path
     * 
     * @return formula path.
     */
    static String locateUniverseFormula() {
        File[] fileList = getFileList(DIRECTORY_PATH);
        for (File file : fileList) {
            if(FILE_NAME.equals(file.getName())){
                return file.getPath();
            }
        }
        return null;
    }

    /**
     * Get list of File present directory path.
     * 
     * @param dirPath
     * @return files
     */
    private static File[] getFileList(String dirPath) {

        File dir = new File(dirPath);

        File[] fileList = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith("");
            }
        });
        return fileList;
    }

    public static void main(String[] args) {
        System.out.println("In this example, the formula path is:");

        System.out.println("and you found it into:");
        System.out.println(A.locateUniverseFormula());
    }
}