package com.oodrive;

import java.util.Date;

final class  Person {

    private final String name;
    private final Date birthDate;

    @SuppressWarnings("unused")
    private Person() {
        this(null,null);
    }

    public Person(String name, Date birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return new Date(birthDate.getTime());
    }
}
